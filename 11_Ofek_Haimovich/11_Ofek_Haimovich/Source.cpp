#include <iostream>
#include <windows.h>
#include <ctime>
#include "philosopher.h"

#define NUM_OF_PHILOSOPHERS 5
int numOfPhilosopher = 0;
float timeForEating[NUM_OF_PHILOSOPHERS] = { 0 };

CRITICAL_SECTION cs[5];
DWORD WINAPI threading(LPVOID lParam);


int main(void)
{
	philosopher* p[NUM_OF_PHILOSOPHERS];
	HANDLE WINAPI thread[NUM_OF_PHILOSOPHERS];

	for (int i = 0; i < NUM_OF_PHILOSOPHERS; i++)
	{
		InitializeCriticalSection(&cs[i]);
		p[i] = new philosopher(i + 1, i, i + 1);
	}
	for (int i = 0; i < NUM_OF_PHILOSOPHERS; i++)
	{
		thread[i] = CreateThread(NULL, 0, threading, p[i], 0, NULL);
	}
	for (int i = 0; i < NUM_OF_PHILOSOPHERS; i++)
	{
		WaitForSingleObject(thread[i], INFINITE);
		CloseHandle(thread[i]);
	}

	for (int i = 0; i < NUM_OF_PHILOSOPHERS; i++)
	{
		std::cout << "time for philosopher number " << i + 1 << ": " << timeForEating[i] << std::endl;
	}

	return 0;
}

DWORD WINAPI threading(LPVOID lParam)
{
	clock_t clock1, clock2;
	int i = 0;
	clock1 = clock();

	while (i < 1000)
	{
		if (TryEnterCriticalSection(&cs[((philosopher*)lParam)->getChopstick(0)]))
		{

			if (TryEnterCriticalSection(&cs[((philosopher*)lParam)->getChopstick(1)]))
			{
				std::cout << ((philosopher*)lParam)->getPhilosopherNum() << " is eating right now" << std::endl;
				LeaveCriticalSection(&cs[((philosopher*)lParam)->getChopstick(1)]);
			}
			
			else
			{
				i--;
			}

			LeaveCriticalSection(&cs[((philosopher*)lParam)->getChopstick(0)]);
		}
		else
		{
			i--;
		}
		i++;
	}

	clock2 = clock();

	timeForEating[numOfPhilosopher] = (float)(clock2 - clock1) / CLOCKS_PER_SEC;
	numOfPhilosopher++;

	return 1;
}