#include "philosopher.h"

philosopher::philosopher(int philosopherNum, int chopstickNum1, int chopstickNum2)
{
	this->philosopherNum = philosopherNum;
	this->theChopsticks[0] = chopstickNum1;
	this->theChopsticks[1] = chopstickNum2;
}
int philosopher::getChopstick(int chopstickNum)
{
	if (this->theChopsticks[chopstickNum] == 5)
	{
		return this->theChopsticks[chopstickNum - 1];
	}
	return this->theChopsticks[chopstickNum];
}
int philosopher::getPhilosopherNum()
{
	return this->philosopherNum;
}