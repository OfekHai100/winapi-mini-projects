#pragma once
class philosopher
{
private:
	int theChopsticks[2];
	int philosopherNum;
public:
	philosopher(int philosopherNum, int chopstickNum1, int chopstickNum2);
	int getPhilosopherNum();
	int getChopstick(int chopstickNum);
};