org 100h

mov	ax, 10 ;can be changed
mov dx, 1
mov bx, 0 ;counter

forward:          
shr	ax, 1 ;move one bit right
jc count ;check carry flag           
jmp fin
        
count:                
inc bx 

fin:          
inc dx
cmp dx, 16 
jbe forward ;next bit check
mov ax, bx
call print_num
    

mov  ah, 0 
int  16h
ret     
include magshimim.inc