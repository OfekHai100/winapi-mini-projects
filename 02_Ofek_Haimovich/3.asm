org 100h
       
jmp start
RES DB ?
CNT DB 20       ; Initialize the counter for the no of Fibonacci No needed
START: MOV AX,@DATA
MOV DS,AX
LEA SI,RES
MOV CL,CNT       ; Load the count value for CL for looping
MOV AX,00H       ; Default No
MOV BX,01H       ; Default No

;Fibonacci Part
L1:ADD AX,BX
MOV [SI],AX
MOV AX,BX
MOV BX,[SI]
INC SI
call    print_num
PRINTN
LOOP L1

mov ah, 0
int 16h
ret   
include magshimim.inc