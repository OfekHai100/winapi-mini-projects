#include <windows.h>
#include <iostream>

#define FILENAME "gibrish.bin"
#define PROC_FILE "ex2_part1.exe"
#define BUFFER_SIZE 256

int main()
{
	HANDLE hFile, hMapFile;
	LPCSTR pFileName = FILENAME;
	DWORD file_size;

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	char param[] = PROC_FILE;

	hFile = CreateFileA(pFileName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		//std::cout << "hFile: " << GetLastError() << std::endl;
	}

	file_size = GetFileSize(hFile, NULL);
	hMapFile = CreateFileMappingA(hFile, NULL, PAGE_READWRITE, 0, 0, "myFile");
	if (hMapFile == NULL)
	{
		//std::cout << "hMapfile: " << GetLastError() << std::endl;
	}

	Sleep(50);

	if (!CreateProcess(NULL, param, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
	{
		std::cout << "processError: " << GetLastError() << std::endl;
	}

	Sleep(100);

	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	CloseHandle(hMapFile);
	CloseHandle(hFile);

	return 0;
}