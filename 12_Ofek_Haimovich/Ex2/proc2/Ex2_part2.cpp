#include <windows.h>
#include <iostream>

#define PROC_NAME "myFile"
#define REPLACE_LETTER 'Z'

int main()
{

	HANDLE hMapFile;
	char* pBuf;
	hMapFile = OpenFileMappingA(FILE_MAP_WRITE, true, PROC_NAME);
	if (!hMapFile)
	{
		std::cout << "hMapFile: " << GetLastError() << std::endl;
	}

	pBuf = (char*)MapViewOfFile(hMapFile, FILE_MAP_WRITE, 0, 0, 1);
	if (!pBuf)
	{
		std::cout << "pBuf: " << GetLastError() << std::endl;
	}
	pBuf[0] = REPLACE_LETTER;


	CloseHandle(hMapFile);
	UnmapViewOfFile(pBuf);

	return 0;
}