#include <iostream>
#include <Windows.h>
using namespace std;

int main()
{
	DWORD dwDesiredAccess = GENERIC_READ;
	DWORD dwShareMode = FILE_SHARE_READ;
	DWORD dwCreationDispostion = OPEN_EXISTING;
	DWORD dwFlagsAndAttributes = FILE_ATTRIBUTE_READONLY;

	HANDLE hFile;
	hFile = CreateFile(
		"testRead.txt",
		dwDesiredAccess,
		dwShareMode,
		NULL,
		dwCreationDispostion,
		dwFlagsAndAttributes,
		NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		printf("Unable to create file, error: %d", GetLastError());
		return 0;
	}

	constexpr size_t BUFSIZE = 256;
	char buffer[BUFSIZE];
	DWORD dwBytesToRead = BUFSIZE - 1;
	DWORD dwBytesRead = 0;

	BOOL bRlt = ReadFile(
		hFile,
		(void*)buffer,
		dwBytesToRead,
		&dwBytesRead,
		NULL);

	if (dwBytesRead > 0)
	{
		buffer[dwBytesRead] = '\0';
		cout << "File contents: " << buffer << endl;
	}

	CloseHandle(hFile);

	system("PAUSE");
	return 0;
}