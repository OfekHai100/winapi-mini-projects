org 100h

jmp main
	chrs db  'M','I','M','I','H','S','G','A','M','$'

main:
	mov bp, sp

	mov ax,offset chrs
	call print_ax_str 
	mov ax, offset chrs
	push ax
	push 9
	call reverse
	mov ax,offset chrs
	call print_ax_str 
	
	jmp stop

reverse proc
	; First save the old BP  
	push bp	
	; Now establish new BP 
	mov bp, sp  
	;make space for 2 local variables
	sub sp, 4

	;write your code here:
    mov di, [bp+4] ;save the length of the string
    mov si, di
    dec si ;the "real" length of the string (in order to get index)
    mov ax, di
    mov bx, 2
    div bx 
    mov cx, ax
    mov bx, [bp+6]
    mov di, 0
    
    again:              
        mov al, bx[di]
        mov dl, bx[si]
        mov bx[si], al
        mov bx[di], dl
        dec si
        inc di
        loop again
        
    PRINTN   
        


	mov sp, bp 
	; Restore OLD BP 
	pop bp
	retn 4 
reverse endp

stop:
	mov ah, 0 
	int 16h 
	ret

include magshimim.inc