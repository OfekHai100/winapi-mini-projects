org  100h

jmp main
	
	toSwap1 	db 'a'
	toSwap2 	db 'b'
	result	 	dw ?
	numToNeg	dw -9
	string      db  'm', 'a', 'g', 's', 'h', 'i', 'm', 'i', 'm', 'v', 'e', 'n', 'e', 'h', 'e', 'n', 'i', 'm' ,0Dh,0Ah,'$'
	array		db "0000", 0Dh,0Ah, 24h ; line feed   return   and  stop symbol 24h=$ (ASCII).   
	num1        dw 0xAC45


main:                    
    ;---fun1---;
    PRINTN "2's complement of: " 
    mov ax, numToNeg
    call print_num
    PRINTN
    
    push numToNeg
    call fun1
    pop numToNeg
            
    
    PRINTN "is: "
    call print_num  
    PRINTN
    PRINTN
    
    ;---fun2---;        
    push ax ;save old ax
    
    mov al, toSwap1     
    
    mov ah, 0 
    
    PRINTN "values before swap: "
    PRINTN " toSwap1 = "
    call print_al_chr
    PRINTN
    
    mov al, toSwap2
    
    PRINTN " toSwap2 = "
    call print_al_chr
    PRINTN
    PRINTN
    
    push offset toSwap1 ;save old toSwap1
    push offset toSwap2 ;save old toSwap2
    call fun2  
    
    mov al, toSwap1
    
    PRINTN "values after swap: "
    PRINTN " toSwap1 = "
    call print_al_chr
    PRINTN
    
    mov al, toSwap2  
    
    PRINTN " toSwap2 = "
    call print_al_chr
    PRINTN
    
    ;---fun3---;
    push offset array ;save old array
    push num1 ;save old hexa number value
    push dx
    
    call fun3
    
    pop dx
    
    pop num1
    
    mov ax, offset array ;new array
    PRINTN "Hex string representation is:"
    call print_ax_str
    PRINTN 
    
    ;---fun4---;   
    PRINTN "string before:"
    mov ax, offset string
    push ax
     
    call print_ax_str
    PRINTN  
    
    call fun4      
    
    PRINTN "string after:"
    mov ax, offset string
    call print_ax_str
    PRINTN
    
    jmp finish          
 
 
;This function gets a param of a number and makes the number negative.
;convention: STDCall    
fun1 proc
    num_param equ 4
    
    push bp ;save old bp
    mov bp, sp ;set bp
    sub sp, 2  
    
    mov ax, [bp+num_param]
    not ax 
    inc ax      
    
    mov sp, bp 
    pop bp ;load old bp 
    
    retn 2            
fun1 endp 
  
  
;This function swap between two values (toSwap1 and toSwap2).
;convention: STDCall 
fun2 proc
    swap1_param equ 4
    swap2_param equ 6
    
    push bp ;save old bp                 
    mov bp,sp ;set bp  
    
    mov bx, [bp+swap1_param] ;save toSwap1 ptr
    mov ah, [bx] 
    mov bx, [bp+swap2_param] ;save toSwap2 ptr 
    
    ;save again in order to get the values easy
    mov al, [bx]
    mov cl, al
    mov al, ah
    mov ah, cl
    
    ;swapping the registers
    mov si, [bp+swap1_param]
    mov [si], ah
    mov si, [bp+swap2_param]
    mov [si], al
    
    pop ax ;load old ax
    mov sp,bp 
    pop bp ;load old bp 
     
    retn 4              
fun2 endp 
         
;This function converts hex digit to char
;convention: STDCall
hex_to_char proc
    hex_param equ 4
    last_param equ 9h
    
    push bp ;save old bp
    mov bp, sp ;set bp
    
    mov al, last_param ;last digit in char
    cmp [bp+hex_param], al ;compare the hex digit to the last digit
    mov al, [bp+hex_param] 
    jg letter ;it is a letter digit
    jmp number ;it is a number digit
    
    letter:
        add al, 55 ;ascii letter value
        jmp fin
        
    number:
        add al, 48 ;ascii number value
    
    fin:
        pop bp ;load old bp
        retn 2   
    
hex_to_char endp

;This function converts an hex value to ascii, it makes the hex value a string in array
;convention: STDCall
fun3 proc
    num1_param equ 8
    array_param equ 10
    
    push bx ;save old bx
    push bp ;save old bp
    mov bp, sp ;set bp
    
    xor cl, cl ;cl is zero
    
    mov bx, [bp+array_param] ;save array in bx
     
    ;every iteration, this loop gets a digit of the hex value and calls the "hex_to_char" function with the digit as a parameter
    nextDig:
        mov dx, [bp+num1_param] ;save num1 in dx
        shl dx, cl
        shr dx, 12
        push dx
        call hex_to_char
        mov [bx], al
        inc bx 
        add cl, 4
        cmp cl, 16 ;hex max digit value
        jne nextDig
             
    pop bp ;load old bp
    pop bx ;load old bx  
    retn 4
fun3 endp

;This function sorts the string array by bubble sorting
fun4 proc
    push bp ;save old bp
    mov bp, sp ;set bp
    
    sub sp, 2 ;save memory for one local variable
    
    mov bx, [bp+6]   
    mov di, 0
    mov cx, 0FFFFh ;biggest number 
    
    get_length:          
            
        cmp [[bx]+di], '$'
        je break
        inc di
    loop get_length
    break:
    
    sub di, 1
    mov [bp-2], di
    mov cx, [bp-2]
 
    
    sorting:     
    push cx
    mov cx, [bp-2] 
    
    in_sort: 
    cmp cx, 0
    je fin1
    
    mov di, cx 
    
    mov al, byte ptr[bx+di] 
    
    mov ah, byte ptr[bx+di-1] 
    
    cmp ah, al
    jl no_swap 
    
    pusha
    
    lea ax, [bx+di]
     
    push ax   
    
    lea ax,  [bx+di-1]  
    
    push ax
    
    call fun2 ;use the swap function in order to sort the string array
    popa 
     
    no_swap:
    loop in_sort     
    
    fin1:
    pop cx ;load old cx
    loop sorting
    
    mov sp, bp 
    pop bp ;load old bp
    retn 2        
fun4 endp

finish:
    mov ah, 0
    int 16h
    ret

 
include magshimim.inc 