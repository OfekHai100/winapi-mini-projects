org 100h

jmp start

	result	 	dw ?
	num0		dw 81 ; param1 - the value can be changed
	num1		dw 109 ; param2 - the value can be changed      


start:   
    call main
      
main proc      
    mov cx, 0 ; counter that counts the function calls so it knows when the min function was called once  
    push num0
    push num1  
    call max
    next: 
        PRINTN
        pop result  
        pop bp ; new bp and result will be pushed in the next function
        cmp cx, 1 ; the max function was alread called once
        jne finish
        call min
        
main endp
     

max proc        
        
    push bp
    mov bp,sp 
    push offset result  
    
    sub sp, 6 ; saves memory for the two nums and the result  
    
    mov     ax, [bp+6] ; num0

    mov     bx, [bp+4] ; num1   
                 
    inc cx ; adds a call to the counter
                 
    PRINTN "The bigger number is:"
    cmp ax,bx
        
    ja ax_print ; num0 is bigger (ax) 
    jmp bx_print ; num1 is bigger (bx) 
max endp 

min proc        
        
    push bp
    mov bp,sp 
    push offset result  
    
    sub sp, 6 ; saves memory for the two nums and the result
    
    mov     ax, [bp+14] ; num0

    mov     bx, [bp+12] ; num1
       
    inc cx ; adds a call to the counter    
         
    PRINTN "The smaller number is:"
    cmp ax,bx
        
    ja bx_print ; num1 is smaller
    jmp ax_print ; num0 is smaller
min endp
         
    bx_print:                 
        
         mov [bp+2],bx 
         mov ax,[bp+2]
         call print_num
       
         jmp next
    
    ax_print:
      
        mov [bp+2],ax         
        mov ax,[bp+2]
        call print_num
        
        jmp next
        
    
    finish: ; clean (pop) label   
        
    pop result 
    pop bp 
    pop num1
    pop num0   



mov ah, 0
int 16h
ret          
include magshimim.inc 