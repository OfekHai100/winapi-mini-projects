org 100h

; set data segment to code segment:
mov ax, cs
mov ds, ax
mov cl, 80 ; target temp

start:
in al, 125

cmp al, cl
je finish  

;mov ah, 6
;mov dl, al
;int 21h       ; output character

PRINT "Current temp: "     
push ax ; saving the ax because it is used in the print function
xor ah, ah
call print_num
PRINT " "
pop ax

PRINT "Target temp: "     
push ax ; saving the ax because it is used in the print function
mov al, cl
xor ah, ah
call print_num
PRINTN
PRINTN
pop ax  

mov ah, 06h ; input of the user
mov dl, 0FFh
int 21h 
 
push ax
xor ah, ah

cmp al, '+' 
je next_plus
cmp al, '-' 
je next_minus
jmp next 

next_plus:
    PRINT "+ pressed"
    mov al, 1
    out 127, al   ; turning heater on 
    jmp next

next_minus:
    PRINT "- pressed"
    mov al, 0
    out 127, al   ; turning heater off
    
next:    

;call print_al_chr
PRINTN
PRINTN
pop ax  

jmp start   ; endless loop in order to make sure the thermometer is +60  

finish:
PRINTN "Got the target temp!"  
mov ah, 0
int 16h
ret     
    
;include "01_print_chars.asm"
include magshimim.inc
