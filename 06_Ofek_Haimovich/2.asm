org 100h  
jmp start

start:
    call change

    mov ax, 6
    mov bl, 0      
    
    ;printing all of the operands before the interrupt
    PRINTN "Before: "
    PRINT "ax: "
    call print_num 
    PRINTN
    
    push ax ; saving sax value before using ax to print 
     
    PRINT "bx: "
    mov ax, bx
    call print_num
    PRINTN  
    
    PRINT "cx: "
    mov ax, cx
    call print_num
    PRINTN
    
    PRINT "dx: "
    mov ax, dx
    call print_num 
    PRINTN
    
    pop ax ; returning the old value to ax after using it
    
    pusha
    
    div bl ; dividing by zero error 
    
    popa 
    
    ;printing all of the operands after the interrupt
    PRINTN "After: "
    PRINT "ax: "
    call print_num 
    PRINTN 
     
    PRINT "bx: "
    mov ax, bx
    call print_num
    PRINTN  
    
    PRINT "cx: "
    mov ax, cx
    call print_num
    PRINTN
    
    PRINT "dx: "
    mov ax, dx
    call print_num 
    PRINTN
    
    jmp finish
    
;This function changes the interrupt error when dividing by zero
proc change
    push bp ; save old bp
    mov bp, sp ; change to the old bp                    
    
    mov ax,0
    mov es, ax
    mov es:[0], 0343h ; save ip of the error printing function to the interrupt
    mov es:[2], 700h ; save the code segment       
    
    mov sp, bp
    pop bp
    ret
endp change

;This is the function that prints the error when dividing by zero    
proc printing
    cli
    push bp ; save old bp
    mov bp, sp     
    
    PRINTN "ERROR: division by zero" 
    
    mov sp, bp
    pop bp ; change to the old bp
    sti
    iret
endp printing   

finish:
    mov ah, 0
    int 16h
    ret
  
include magshimim.inc