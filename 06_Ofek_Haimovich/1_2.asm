org 100h

; set data segment to code segment:
mov ax, cs
mov ds, ax

start:

in al, 125

cmp al, 60 ; themo is less than +60
jnge  low

cmp al, 60 ; themo is +60
jle  ok
jnle   high

low:
mov al, 1
out 127, al   ; turning heater on
jmp ok

high:
mov al, 0
out 127, al   ; turning heater off

ok:
jmp start   ; endless loop in order to make sure the thermometer is +60


mov ah, 0
int 16h
ret     

